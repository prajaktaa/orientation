**<h2>Git Overview</h2>**

**<h3>What is Git?</h3>**
Git is the way we manage code and there is no software development without git.Git is a version-control system for tracking changes in computer files and coordinating work on those files among multiple people. Git is a Distributed Version Control System. So Git does not necessarily rely on a central server to store all the versions of a project's files.

**<h3>What is Git in nutshell?</h3>**
 This is an important section to absorb, because if you understand what Git is and the fundamentals of how it works, then using Git effectively will probably be much easier for you. As you learn Git, try to clear your mind of the things you may know about other VCSs, such as CVS, Subversion or Perforce — doing so will help you avoid subtle confusion when using the tool. Even though Git’s user interface is fairly similar to these other VCSs, Git stores and thinks about information in a very different way, and understanding these differences will help you avoid becoming confused while using it.

The major difference between Git and any other VCS (Subversion and friends included) is the way Git thinks about its data. Conceptually, most other systems store information as a list of file-based changes. These other systems (CVS, Subversion, Perforce, Bazaar, and so on) think of the information they store as a set of files and the changes made to each file over time (this is commonly described as delta-based version control).

![Check-ins Over Time](https://gitlab.com/vaibhaviparanjpe01/orientation/-/raw/master/extras/vcs.png)

Git doesn’t think of or store its data this way. Instead, Git thinks of its data more like a series of snapshots of a miniature filesystem. With Git, every time you commit, or save the state of your project, Git basically takes a picture of what all your files look like at that moment and stores a reference to that snapshot. To be efficient, if files have not changed, Git doesn’t store the file again, just a link to the previous identical file it has already stored. Git thinks about its data more like a stream of snapshots.

This is an important distinction between Git and nearly all other VCSs. It makes Git reconsider almost every aspect of version control that most other systems copied from the previous generation. This makes Git more like a mini filesystem with some incredibly powerful tools built on top of it, rather than simply a VCS. We’ll explore some of the benefits you gain by thinking of your data this way when we cover Git branching in Git Branching.

**<h3>Git terminologies</h3>**
**Respository**:<br>
A working tree in a Git Repository is the collection of files which are originated form a certain version of the repository. It helps in tracking the changes done by a specific user on one version of the repository.Often called as a *repo*. A repository is the collection of files and folders (code files) that you’re using git to track. It’s the big box you and your team throw your code into. Whenever an operation is committed by the user, Git will look only for the files which are present in the working area, and not all the modified files. Only the files which are present in the working area are considered for commit operation.
The user of the working tree gets to change the files by modifying existing files and removing or creating files.
There are a few stages of a file in the working tree of a repository:<br>

1. **Untracked**: In this stage, the Git repository is unable to track the file, which means that the file is never staged nor it is committed.<br>
2. **Tracked**: When the Git repository tracks a file, which means the file is committed but is not staged in the working directory.<br>
3. **Staged**: In this stage, the file is ready to be committed and is placed in the staging area waiting for the next commit.<br>
4. **Modified/Dirty**: When the changes are made to the file i.e. the file is modified but the change is not yet staged.<br>

**Gitlab**:<br>
GitLab is a web-based DevOps lifecycle tool that provides a Git-repository manager providing wiki, issue-tracking and continuous integration/continuous deployment pipeline features, using an open-source license, developed by GitLab Inc. 

**Commit**:<br>
The *commit* command only saves a new commit object in the local Git repository. Exchanging commits has to be performed manually and explicitly (with the *fetch*, *pull*, and *push* commands).

**Push**:<br>
The  *push* command is used to upload local repository content to a remote repository. Pushing is how you transfer commits from your local repository to a remote repo. It's the counterpart to git fetch , but whereas fetching imports commits to local branches, pushing exports commits to remote branches.

**Branch**:<br>
The  *branch* command lets you create, list, rename, and delete branches. It doesn't let you switch between branches or put a forked history back together again. For this reason, git branch is tightly integrated with the git checkout and git merge commands.

**Merge**:<br>
The *merge* is often used in conjunction with git checkout for selecting the current branch and git branch -d for deleting the obsolete target branch.

**Clone**:<br>
The *clone* is a Git command line utility which is used to target an existing repository and create a clone, or copy of the target repository.Cloning a local or remote repository. Cloning a bare repository. Using shallow options to partially clone repositories.

**Fork**:<br>
Fork means you just create a copy of the main repository of a project source code to your own GitHub profile.Then make your changes and create a Pull Request to the main repository branch. If the Main Repository owners like your changes they will merge it to the main repository.

**<h3>How to Install Git?</h3>**
Git (probably) didn’t come installed on your computer, so we have to get it there. Luckily, installing git is super easy, whether you’re on Linux, Mac, or Windows.

For Linux, open the terminal and type
>sudo apt-get install git<br>

It only works on Ubuntu. If not, there is a list of all the Linux package installation commands for whichever other distro you’re on.

**<h3>Git Internals</h3>**
**Git has three main states that your files can reside in: modified, staged, and committed :**
1. Modified means that you have changed the file but have not committed it to your
   repo yet.
2. Staged means that you have marked a modified file in its current version to go into
   your next picture/snapshot.
3. Committed means that the data is safely stored in your local repo in form of
   pictures/snapshots.


**At one time there are 3/4 different trees of your software code/repository are present.**
1. **Workspace**: All the changes you make via Editor(s) (gedit, notepad, vim, nano) is
   done in this tree of repository.
2. **Staging**: All the staged files go into this tree of your repository.
3. **Local Repository**: All the committed files go to this tree of your repository.
3. **Remote Repository**: This is the copy of your Local Repository but is stored in some
   server on the Internet. All the changes you commit into the Local Repository are
   not directly reflected into this tree. You need to push your changes to the Remote
   Repository.

![Respository](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/extras/Git.png)

**<h3>Git Workflow</h3>**
The basic Git work flow goes something like this:

**Clone the repo**<br>
>$ git clone <link-to-repository> 

**Create a new branch**<br>
>$ git checkout master
>$ git checkout -b <your-branch-name>

**Modify**<br>
>$ git add.                           *#To add untracked files ( . adds all files)*

**Commit**<br>
>$ git commit -sv                     *#Description about the commit*

**Push**
>$ git push origin <branch-name>      *# push changes into repository*