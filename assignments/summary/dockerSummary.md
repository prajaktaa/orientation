**<h2> Docker overview </h2>**

**<h3>Docker</h3>**
Docker is an open source project that makes it easy to create containers and container-based apps.It is a set of platform-as-a-service products that create isolated virtualized environments for building, deploying, and testing applications.

**<h3>The Docker terminologies</h3>**

<h4>1. Docker platform</h4>
Docker provides tooling and a platform to manage the lifecycle of your containers:<br>
![container](https://www.docker.com/sites/default/files/d8/2018-11/docker-containerized-appliction-blue-border_2.png)

1. Develop application using containers.<br>
2. The container becomes the unit for distributing and testing application.<br>
3. Deploy application into  production environment.<br>

This works the same whether your production environment is a local data center, a cloud provider, or a hybrid of the two.

<h4>2. Docker Engine</h4>
Docker Engine is a client-server application with these major components: <br> 

1. A server which is a type of long-running program called a daemon process (the dockerd command). <br>
2. A REST API which specifies interfaces that programs can use to talk to the daemon and instruct it what to do. <br>
3. A command line interface (CLI) client the (docker command.) <br>
 ![Engine Components Flow](https://docs.docker.com/engine/images/engine-components-flow.png)


<h4>3.Docker Image and Container</h4>
A Docker image is contains everything needed to run an applications as a container. <br>
This includes:<br>

1. code<br>
2. runtime<br>
3. libraries<br>
4. environment variables<br>
5. configuration files<br>

A Docker container is a virtualized run-time environment where users can isolate applications from the underlying system. A container is, ultimately, just a running image.<br>
 ![Container_img](https://jfrog--c.documentforce.com/servlet/servlet.ImageServer?id=0151r000006uDeS&oid=00D20000000M3v0&lastMod=1584629589000)

1. One image can create multiple containers.
2. A Docker container is a running Docker image.

<h4>4.Docker Hub</h4>
Docker Hub is a SaaS repository for sharing and managing containers, where you will find official Docker images from open-source projects and software vendors and unofficial images from the general public.

<h4>5.Docker Files</h4>
A Dockerfile is a simple text file that contains a list of commands the Docker client calls (on the command line) when assembling an image.

<h4>6.The Docker daemon</h4>
The Docker daemon (dockerd) listens for Docker API requests and manages Docker objects such as images, containers, networks, and volumes. A daemon can also communicate with other daemons to manage Docker services.

<h4>7.The Docker client</h4>
Docker client uses commands and REST APIs to communicate with the Docker Daemon (Server)

<h4>8.Docker registries</h4>
A Docker registry stores Docker images. Docker Hub is a public registry that anyone can use, and Docker is configured to look for images on Docker Hub by default. You can even run your own private registry.


**<h3>Docker Architecture</h3>**
Docker uses a client-server architecture. The Docker client talks to the Docker daemon, which does the heavy lifting of building, running, and distributing your Docker containers. The Docker client and daemon can run on the same system, or you can connect a Docker client to a remote Docker daemon. The Docker client and daemon communicate using a REST API, over UNIX sockets or a network interface.<br>
![Docker Architecture](https://wiki.aquasec.com/download/attachments/2854889/Docker_Architecture.png?version=1&modificationDate=1520172700553&api=v2)

**<h3>Docker workflow</h3>**
Step-by-step workflow for developing Docker containerized apps:<br>
1. Locally build and test a Docker image on your development box.
2. Build your official image for testing and deployment.
3. Deploy your Docker image to your server.
![Docker workflow](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/docker-application-development-process/media/docker-app-development-workflow/life-cycle-containerized-apps-docker-cli.png)


**<h3>Docker Commands</h3>**
<h4>1.For Containers</h4>
Use docker container my_command<br>

 1. `create` — Create a container from an image.<br>
2. `start` — Start an existing container.<br>
3. `run` — Create a new container and start it.<br>
4. `ls` — List running containers.<br>
5. `inspect` — See lots of info about a container.<br>
6. `logs` — Print logs.<br>
7. `stop` — Gracefully stop running container.<br>
8. `kill` —Stop main process in container abruptly.<br>
9. `rm`— Delete a stopped container.<br>
10. `stop`-stops any running container(s).<br>
11. `ps`-view all the containers that are running on the Docker Host.

<h4>2.For Images</h4>
Use docker image my_command<br>

 1.`build` — Build an image.<br>
2. `push` — Push an image to a remote registry.<br>
3. `ls` — List images.<br>
4. `history` — See intermediate image info.<br>
5. `inspect` — See lots of info about an image, including the layers.<br>
6. `rm` — Delete an image.<br>

**<h3>Common Operations on Docker</h3>**
1. Downloading/pulling the docker images wanted to be worked with.
2. Copying code inside the docker
3. Accessing docker terminal
4. Installing additional required dependencies
5. Compile and Run the Code inside docker
6. Document steps to run program in README.md file
7. Commit the changes done to the docker.
8. Push docker image to the docker-hub and share repository with people who want to try the code.

**<h3>Basic start with Docker commands</h3>**

1. docker start using container ID<br>
>$ docker start 30986

2. docker start using container name<br>
>$ docker start prajaktaa

3. docker stop using container ID<br>
>$ docker stop 30986

4. docker stop using container name<br>
>$ docker stop prajaktaa

5. docker run,creates containers from docker images.<br>
>$ docker run prajaktaa

6. docker rm,deletes containers.<br>
>$ docker rm prajaktaa

**<h3>Docker Features</h3>**
1. Easy and Faster Configuration<br>
2. Increase productivity<br>
3. Application Isolation<br>
4. Swarm<br>
5. Routing Mesh<br>
6. Services<br>


